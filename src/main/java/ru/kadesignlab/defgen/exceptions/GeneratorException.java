package ru.kadesignlab.defgen.exceptions;

public class GeneratorException extends Exception
{
  public GeneratorException(String errorMessage)
  {
    super(errorMessage);
  }

  public GeneratorException(String errorMessage, Throwable cause)
  {
    super(errorMessage, cause);
  }
}
