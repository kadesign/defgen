package ru.kadesignlab.defgen.ui.dialogs;

import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.stage.Window;

import java.util.ArrayList;
import java.util.Optional;

public class FileExistsDialog
{
  public static class Response
  {
    public final static int OVERWRITE=0;
    public final static int RETRY=1;
    public final static int EXIT=2;
  }

  private Dialog<ButtonType> dialog;
  ButtonType overwriteButton;
  ButtonType retryButton;
  ButtonType cancelButton;

  public FileExistsDialog(String filePath)
  {
    dialog=new Dialog<>();
    dialog.getDialogPane().setPrefWidth(600);
    dialog.getDialogPane().setMinHeight(150);

    overwriteButton=new ButtonType("Overwrite", ButtonBar.ButtonData.LEFT);
    retryButton=new ButtonType("Retry", ButtonBar.ButtonData.LEFT);
    cancelButton=new ButtonType("Cancel", ButtonBar.ButtonData.RIGHT);

    ArrayList<ButtonType> buttons=new ArrayList<>();

    dialog.setTitle("File already exists");
    dialog.setHeaderText("Object definition file already exists");
    dialog.setContentText("File " + filePath + " already exists. Please choose one of the available options:");

    buttons.add(overwriteButton);
    buttons.add(retryButton);
    buttons.add(cancelButton);

    dialog.getDialogPane().getButtonTypes().addAll(buttons);

    Window window=dialog.getDialogPane().getScene().getWindow();
    window.setOnCloseRequest(event -> window.hide());
  }

  public int get()
  {
    Optional<ButtonType> result=dialog.showAndWait();

    if (result.isPresent())
    {
      ButtonType pressedButton=result.get();

      if (pressedButton == overwriteButton)
      {
        return Response.OVERWRITE;
      }
      else
        if (pressedButton == retryButton)
        {
          return Response.RETRY;
        }
        else
        {
          return Response.EXIT;
        }
    }
    else
    {
      return Response.EXIT;
    }
  }
}
