package ru.kadesignlab.defgen.ui.dialogs;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

public class ExceptionDialog
{
  private Alert alert;

  public ExceptionDialog(Exception e, String headerText)
  {
    alert=new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Error - DefGen");
    alert.setHeaderText(headerText);
    alert.setContentText(e.getMessage());
    alert.getDialogPane().setPrefWidth(600);

    StringWriter sw=new StringWriter();
    PrintWriter pw=new PrintWriter(sw);
    e.printStackTrace(pw);
    String exceptionText=sw.toString();

    Label label=new Label("Stack trace:");

    TextArea textArea=new TextArea(exceptionText);
    textArea.setEditable(false);
    textArea.setWrapText(true);

    textArea.setMaxWidth(Double.MAX_VALUE);
    textArea.setMaxHeight(Double.MAX_VALUE);
    GridPane.setVgrow(textArea, Priority.ALWAYS);
    GridPane.setHgrow(textArea, Priority.ALWAYS);

    GridPane expContent=new GridPane();
    expContent.setMaxWidth(Double.MAX_VALUE);
    expContent.add(label, 0, 0);
    expContent.add(textArea, 0, 1);

    alert.getDialogPane().setExpandableContent(expContent);
  }

  public Optional<ButtonType> showAndWait()
  {
    return alert.showAndWait();
  }
}
