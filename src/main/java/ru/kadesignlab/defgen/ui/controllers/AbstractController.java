package ru.kadesignlab.defgen.ui.controllers;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public abstract class AbstractController
{
  protected Stage stage;
  protected Scene scene;
  protected Parent parent;

  public void launchScene(Stage stage)
  {
    this.stage=stage;
    stage.setScene(scene);
    stage.setResizable(false);

    stage.show();
  }

  protected void setScene(Scene scene)
  {
    this.scene=scene;
  }

  protected void setParent(Parent parent)
  {
    this.parent=parent;
  }
}
