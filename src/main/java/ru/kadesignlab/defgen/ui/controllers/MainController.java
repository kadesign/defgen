package ru.kadesignlab.defgen.ui.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputControl;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kadesignlab.defgen.Constants;
import ru.kadesignlab.defgen.exceptions.GeneratorException;
import ru.kadesignlab.defgen.generators.PackingListGenerator;
import ru.kadesignlab.defgen.ui.dialogs.ExceptionDialog;
import ru.kadesignlab.defgen.ui.dialogs.FileExistsDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import static ru.kadesignlab.defgen.Constants.LINE_SEPARATOR;

public class MainController extends AbstractController
{
  private static final Logger LOG=LogManager.getLogger(MainController.class);
  private final String FXML_VIEW="/views/MainWindow.fxml";
  private final String WINDOW_TITLE="Convert CSV to DEF - DefGen";

  @FXML
  private TextField pathToCsvFileField;
  @FXML
  private TextField plNameField;
  @FXML
  private TextField plLabelField;
  @FXML
  private TextArea plDescriptionField;
  @FXML
  private TextField plOwnerField;

  @FXML
  private Button selectCsvFileButton;
  @FXML
  private Button generateDefButton;
  @FXML
  private Button exitButton;

  public MainController()
  {
    LOG.debug("Loading FXML for main view from: {}", FXML_VIEW);

    FXMLLoader loader=new FXMLLoader(getClass().getResource(FXML_VIEW));
    loader.setController(this);

    try
    {
      Parent rootNode=loader.load();
      Scene scene=new Scene(rootNode);

      super.setParent(rootNode);
      super.setScene(scene);
    }
    catch (IOException e)
    {
      LOG.error("Can't display window", e);
      throw new RuntimeException(e);
    }
  }

  public void launchScene(Stage stage)
  {
    stage.setTitle(WINDOW_TITLE);
    initWindow();
    super.launchScene(stage);
  }

  // Fields
  @FXML
  private void toggleGenerateButtonAction()
  {
    boolean isPathToCsvNotFilled=pathToCsvFileField.getText() == null || pathToCsvFileField.getText().isEmpty();
    boolean isPlNameNotFilled=plNameField.getText() == null || plNameField.getText().isEmpty();
    boolean isPlOwnerNotFilled=plOwnerField.getText() == null || plOwnerField.getText().isEmpty();

    generateDefButton.setDisable(isPathToCsvNotFilled || isPlNameNotFilled || isPlOwnerNotFilled);
  }

  // Buttons
  @FXML
  private void selectCsvFileAction()
  {
    Stage stage=(Stage) selectCsvFileButton.getScene().getWindow();
    final FileChooser fileChooser=new FileChooser();
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV files", "*.csv"));

    if (pathToCsvFileField.getText() != null && !pathToCsvFileField.getText().equals("")
        && new File(pathToCsvFileField.getText()).isFile())
    {
      fileChooser.setInitialDirectory(new File(pathToCsvFileField.getText()).getParentFile());
    }
    File dir=fileChooser.showOpenDialog(stage);
    if (dir != null)
    {
      pathToCsvFileField.setText(dir.getAbsolutePath());
    }

    toggleGenerateButtonAction();
  }

  @FXML
  private void generateDefAction()
  {
    trimFieldData(pathToCsvFileField);
    trimFieldData(plNameField);
    trimFieldData(plOwnerField);
    trimFieldData(plLabelField);
    trimFieldData(plDescriptionField);

    String csvFile=pathToCsvFileField.getText();
    String plName=plNameField.getText();
    String plOwner=plOwnerField.getText();
    String plLabel=plLabelField.getText();
    String plDescription=plDescriptionField.getText().replaceAll("\n", LINE_SEPARATOR);
    String outputDefPath=new File(csvFile).getParent() + File.separator + plName + Constants.FILE_EXTENSION;

    if (new File(outputDefPath).exists())
    {
      int response=new FileExistsDialog(outputDefPath).get();
      if (response == FileExistsDialog.Response.OVERWRITE)
      {
        getAndWriteDefinitions(outputDefPath, csvFile, plName, plOwner, plLabel, plDescription);
      }
      else
        if (response == FileExistsDialog.Response.RETRY)
        {
          generateDefAction();
        }
    }
    else
    {
      getAndWriteDefinitions(outputDefPath, csvFile, plName, plOwner, plLabel, plDescription);
    }
  }

  @FXML
  private void exitAction()
  {
    Stage stage=(Stage) exitButton.getScene().getWindow();
    stage.close();
  }

  private void initWindow()
  {
    plDescriptionField.addEventFilter(KeyEvent.KEY_PRESSED, key -> {
      if (key.getCode() == KeyCode.TAB)
      {
        key.consume();
        if (key.isShiftDown())
        {
          plLabelField.requestFocus();
        }
        else
        {
          plOwnerField.requestFocus();
        }
      }
    });

    toggleGenerateButtonAction();
  }

  private void getAndWriteDefinitions(String outputDefPath, String csvFile, String plName, String plOwner, String plLabel, String plDescription)
  {
    try (OutputStreamWriter writer=new OutputStreamWriter(new FileOutputStream(outputDefPath), Constants.OUTPUT_ENCODING))
    {
      PackingListGenerator plGenerator=new PackingListGenerator(csvFile, plName, plOwner, plLabel, plDescription);
      writer.write(plGenerator.generate());
      Runtime.getRuntime().exec("explorer.exe /select," + outputDefPath);
    }
    catch (IOException | GeneratorException e)
    {
      LOG.error(e, e);
      new ExceptionDialog(e, "Can't generate object definition file").showAndWait();
    }
  }

  private void trimFieldData(TextInputControl field)
  {
    if (field.getText() != null)
    {
      field.setText(field.getText().trim());
    }
  }
}
