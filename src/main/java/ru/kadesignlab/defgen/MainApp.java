package ru.kadesignlab.defgen;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.kadesignlab.defgen.exceptions.GeneratorException;
import ru.kadesignlab.defgen.generators.PackingListGenerator;
import ru.kadesignlab.defgen.ui.controllers.MainController;

import java.io.*;
import java.util.Properties;

public class MainApp extends Application
{
  public static void main(String[] args)
  {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage)
  {
    new MainController().launchScene(primaryStage);
  }
}
