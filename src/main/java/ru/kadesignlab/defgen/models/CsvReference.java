package ru.kadesignlab.defgen.models;

import ru.kadesignlab.defgen.enums.ReferenceType;

public class CsvReference
{
  private String name;
  private ReferenceType type;

  public CsvReference(String name, String type)
  {
    if (name == null || type == null)
      throw new NullPointerException("Reference attributes can't be equal to null");
    this.name=name;
    this.type=ReferenceType.fromString(type);
  }

  public String getName()
  {
    return name;
  }

  public ReferenceType getType()
  {
    return type;
  }

  @Override public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    CsvReference other=(CsvReference) obj;
    return (name.equals(other.name) && type == other.type);
  }

  @Override public int hashCode()
  {
    return name.hashCode() + type.hashCode();
  }
}
