package ru.kadesignlab.defgen.generators;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ru.kadesignlab.defgen.MainApp;
import ru.kadesignlab.defgen.enums.ReferenceType;
import ru.kadesignlab.defgen.exceptions.GeneratorException;
import ru.kadesignlab.defgen.models.CsvReference;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static ru.kadesignlab.defgen.Constants.LINE_SEPARATOR;

public class PackingListGenerator extends AbstractGenerator
{
  private static final Logger LOG=LogManager.getLogger(MainApp.class);

  private String pathToCsv;
  private String name;
  private String owner;
  private String label;
  private String description;

  public PackingListGenerator(final String pathToCsv, final String name, final String owner, final String label, final String description)
  {
    LOG.debug("New PackingListGenerator from: pathToCsv={}, name={}, owner={}, label={}, description={}", pathToCsv, name, owner, label, description);
    this.pathToCsv=pathToCsv;
    this.name=name;
    this.owner=owner;
    this.label=label;
    this.description=description;
  }

  String getHeader(final String plName, final String owner, final String label, final String description, final int refCount)
  {
    LOG.trace("+getHeader()");
    final Date curDate=new Date();

    LOG.trace("-getHeader()");
    return "begin container" + LINE_SEPARATOR
        + "   name           : " + plName + LINE_SEPARATOR
        + "   type           : 3" + LINE_SEPARATOR
        + "   num-references : " + refCount + LINE_SEPARATOR
        + "   timestamp      : " + curDate.getTime() / 1000 + LINE_SEPARATOR
        + "   owner          : " + owner + LINE_SEPARATOR
        + "   last-changed   : " + owner + LINE_SEPARATOR
        + "   export-version : 12" + LINE_SEPARATOR
        + "   label          : " + label + LINE_SEPARATOR
        + "   description    : " + description.replace(LINE_SEPARATOR, "\r\u0001") + LINE_SEPARATOR;
  }

  String getReferenceText(final String name, final ReferenceType type)
  {
    LOG.trace("+getReferenceText()");
    LOG.debug("Params: name={}, type={}", name, type.name());

    switch (type)
    {
      case ACTIVE_LINK_GUIDE:
      case FILTER_GUIDE:
      case APPLICATION:
      case PACKING_LIST:
      case WEB_SERVICE:
        LOG.trace("-getReferenceText()");
        return "reference {" + LINE_SEPARATOR
            + "   type           : " + type.getArType() + LINE_SEPARATOR
            + "   datatype       : 1" + LINE_SEPARATOR
            + "   value          : 0\\" + LINE_SEPARATOR
            + "}" + LINE_SEPARATOR
            + "reference {" + LINE_SEPARATOR
            + "   type           : 6" + LINE_SEPARATOR
            + "   datatype       : 0" + LINE_SEPARATOR
            + "   object         : " + name.trim() + LINE_SEPARATOR
            + "}" + LINE_SEPARATOR;
      case SCHEMA:
      case ACTIVE_LINK:
      case FILTER:
      case ESCALATION:
      case CHAR_MENU:
      case IMAGE:
      case ASSOCIATION:
        LOG.trace("-getReferenceText()");
        return "reference {" + LINE_SEPARATOR
            + "   type           : " + type.getArType() + LINE_SEPARATOR
            + "   datatype       : 0" + LINE_SEPARATOR
            + "   object         : " + name.trim() + LINE_SEPARATOR
            + "}" + LINE_SEPARATOR;
      case DISTRIBUTED_MAPPING:
      case DISTRIBUTED_POOL:
        LOG.trace("-getReferenceText()");
        return "reference {" + LINE_SEPARATOR
            + "   type           : " + type.getArType() + LINE_SEPARATOR
            + "   datatype       : 1" + LINE_SEPARATOR
            + "   label          : " + name.trim() + LINE_SEPARATOR
            + "   value          : 0\\" + LINE_SEPARATOR
            + "}" + LINE_SEPARATOR;
      default:
        throw new IllegalArgumentException("Invalid reference type");
    }
  }

  private void validateCsv(List<CSVRecord> records) throws GeneratorException
  {
    for (CSVRecord record : records)
    {
      if (record.size() != 2)
      {
        throw new GeneratorException("CSV list is invalid: column count is not equal to 2");
      }

      try
      {
        ReferenceType.fromString(record.get(0));
      }
      catch (IllegalArgumentException e)
      {
        throw new GeneratorException("CSV list is invalid: bad file structure", e);
      }
    }
  }

  @Override
  public String generate() throws GeneratorException
  {
    LOG.trace("+generate()");
    List<ReferenceType> complexTypes=new ArrayList<>();
    complexTypes.add(ReferenceType.ACTIVE_LINK_GUIDE);
    complexTypes.add(ReferenceType.FILTER_GUIDE);
    complexTypes.add(ReferenceType.APPLICATION);
    complexTypes.add(ReferenceType.PACKING_LIST);
    complexTypes.add(ReferenceType.WEB_SERVICE);

    try (FileReader reader=new FileReader(pathToCsv))
    {
      final StringBuilder output=new StringBuilder(super.generateCommonHeader());
      final CSVParser csvParser=new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(';'));
      Set<CsvReference> set=new LinkedHashSet<>();
      AtomicInteger recordsAmount=new AtomicInteger();
      List<CSVRecord> records=csvParser.getRecords();

      validateCsv(records);
      records.forEach(record -> {
        CsvReference csvReference=new CsvReference(record.get(1), record.get(0));
        set.add(csvReference);
        if (complexTypes.contains(csvReference.getType()))
        {
          recordsAmount.addAndGet(2);
        }
        else
        {
          recordsAmount.getAndIncrement();
        }
      });

      output.append(getHeader(name, owner, label, description, recordsAmount.get()));
      set.forEach((reference) -> output.append(getReferenceText(reference.getName(), reference.getType())));
      output.append(getFooter());

      LOG.trace("-generate()");
      return output.toString();
    }
    catch (IOException e)
    {
      throw new GeneratorException("I/O error", e);
    }
  }
}
