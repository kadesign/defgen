package ru.kadesignlab.defgen.generators;

import ru.kadesignlab.defgen.exceptions.GeneratorException;

import java.text.SimpleDateFormat;
import java.util.Date;

import static ru.kadesignlab.defgen.Constants.LINE_SEPARATOR;

public abstract class AbstractGenerator
{
  public abstract String generate() throws GeneratorException;

  protected String generateCommonHeader()
  {
    final SimpleDateFormat format=new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
    final Date curDate=new Date();

    return "char-set: UTF-8" + LINE_SEPARATOR
        + "#" + LINE_SEPARATOR
        + "#  Generated with DefGen at " + format.format(curDate) + LINE_SEPARATOR
        + "#" + LINE_SEPARATOR;
  }

  String getFooter()
  {
    return "end" + LINE_SEPARATOR;
  }
}
