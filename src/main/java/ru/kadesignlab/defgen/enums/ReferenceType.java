package ru.kadesignlab.defgen.enums;

public enum ReferenceType
{
  SCHEMA("Form", 2),
  FILTER("Filter", 3),
  ESCALATION("Escalation", 4),
  ACTIVE_LINK("Active link", 5),
  CHAR_MENU("Menu", 7),
  IMAGE("Image", 8),
  ASSOCIATION("Association", 10),
  ACTIVE_LINK_GUIDE("Active link guide", 32783),
  APPLICATION("Application", 32784),
  PACKING_LIST("Packing list", 32785),
  DISTRIBUTED_MAPPING("Distributed mapping", 32787),
  DISTRIBUTED_POOL("Distributed pool", 32792),
  FILTER_GUIDE("Filter guide", 32793),
  WEB_SERVICE("Web service", 32801);

  private final String name;
  private final int arType;

  ReferenceType(String name, int arType)
  {
    this.name=name;
    this.arType=arType;
  }

  public int getArType()
  {
    return this.arType;
  }

  public static ReferenceType fromString(String text)
  {
    for (ReferenceType r : ReferenceType.values())
    {
      if (r.name.equals(text))
      {
        return r;
      }
    }
    throw new IllegalArgumentException("No reference type with name " + text + " found");
  }
}
