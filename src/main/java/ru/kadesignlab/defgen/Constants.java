package ru.kadesignlab.defgen;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Constants
{
  public static final String FILE_EXTENSION=".def";
  public static final String LINE_SEPARATOR=System.lineSeparator();
  public static final Charset OUTPUT_ENCODING=StandardCharsets.UTF_8;
}
