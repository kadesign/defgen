# DefGen
[![Version](https://img.shields.io/badge/version-v1.1.1-blue)](https://bitbucket.org/kadesign/defgen/-/tags/v1.1.1)
[![Pipeline status](https://gitlab.com/kadesign/defgen/badges/master/pipeline.svg)](https://gitlab.com/kadesign/defgen/-/commits/master)
![Is maintained](https://img.shields.io/maintenance/yes/2021)

Приложение для генерации файлов описания объектов BMC Remedy AR System из простого списка имен объектов

## Требования
- Java 11 (тестировалось на сборке Azul Zulu)
- Maven (для сборки)

## Правила создания валидного списка объектов
1. Принимаются только списки в формате `.csv`
2. Разделитель столбцов - `;` (точка с запятой)
3. Должно быть только две колонки в следующем порядке: `Тип объекта`, `Имя объекта`
4. Допустимые типы объектов:
    - `Form`
    - `Filter`
    - `Escalation`
    - `Active link`
    - `Menu`
    - `Image`
    - `Association`
    - `Active link guide`
    - `Application`
    - `Packing list`
    - `Distributed mapping`
    - `Distributed pool`
    - `Filter guide`
    - `Web service`